#ifndef GAZEBO_PLUGINS_HEAT_MAZE_HH_
#define GAZEBO_PLUGINS_HEAT_MAZE_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

#include "ros/ros.h"
#include "rodentrider_arena/SpawnModel.h"
#include "rodentrider_arena/RemoveModel.h"
#include "std_srvs/Trigger.h"

namespace gazebo
{
  class GAZEBO_VISIBLE MyPlugin : public WorldPlugin
  {
    // Constructor
    public: MyPlugin();
    
    // Deconstructor
    public: virtual ~MyPlugin();

    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
  
    // Animations
    private: virtual void Spin();

    // World Reset Callback
    public: virtual void OnReset();

    private: virtual void LoadMaze(sdf::ElementPtr _sdf);

    private: virtual bool SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z, double yaw);
    private: virtual bool ServiceSpawnModel(rodentrider_arena::SpawnModel::Request &req, rodentrider_arena::SpawnModel::Response &res);

    private: virtual bool RemoveModel(std::string model_name);
    private: virtual bool ServiceRemoveModel(rodentrider_arena::RemoveModel::Request &req, rodentrider_arena::RemoveModel::Response &res);

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection, resetConnection;

    private: std::unique_ptr<ros::NodeHandle> rosNode;

    private: ros::ServiceServer removeModelService;
    private: ros::ServiceServer spawnModelService;

    private: physics::WorldPtr world;
  };
}
#endif
