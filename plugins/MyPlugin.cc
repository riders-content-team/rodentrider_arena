#include "MyPlugin.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(MyPlugin)

MyPlugin::MyPlugin() : WorldPlugin() // Constructor
{
}

MyPlugin::~MyPlugin() // Deconstructor
{
}

// Runs once on initialization
void MyPlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;
    this->LoadMaze(_sdf);


    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "spawn_model_service_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());

    this->removeModelService = this->rosNode->advertiseService<rodentrider_arena::RemoveModel::Request, rodentrider_arena::RemoveModel::Response>
      ("remove_model", std::bind(&MyPlugin::ServiceRemoveModel, this, std::placeholders::_1, std::placeholders::_2));

    this->spawnModelService = this->rosNode->advertiseService<rodentrider_arena::SpawnModel::Request, rodentrider_arena::SpawnModel::Response>
      ("spawn_model", std::bind(&MyPlugin::ServiceSpawnModel, this, std::placeholders::_1, std::placeholders::_2));

    // Connect to the world update event.
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
            std::bind(&MyPlugin::Spin, this));

    // Connect to the world reset event.
    this->resetConnection = event::Events::ConnectWorldReset(
            std::bind(&MyPlugin::OnReset, this));
}

void MyPlugin::Spin()
{
  if (ros::ok()) {
    ros::spinOnce();
  }
}

void MyPlugin::OnReset()
{
}

void MyPlugin::LoadMaze( sdf::ElementPtr sdf)
{
}

bool MyPlugin::SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z, double yaw)
{
    std::string pose_x_str = std::to_string(pose_x);
    std::string pose_y_str = std::to_string(pose_y);
    std::string pose_z_str = std::to_string(pose_z);
    std::string pose_yaw_str = std::to_string(yaw);

    std::string sdf_file_string = "<?xml version='1.0' ?> "
                                  "<sdf version='1.5'>"
                                  "<include>"
                                  "<pose>" + pose_x_str + " " + pose_y_str + " " + pose_z_str + " 0 0 " + pose_yaw_str + "</pose>"
                                  "<name> " + spawn_name + "</name>"
                                  "<uri>model://" + model_name + "</uri>"
                                  "</include>"
                                  "</sdf>";

    this->world->InsertModelString(sdf_file_string);
    return true;
}

bool MyPlugin::ServiceSpawnModel(rodentrider_arena::SpawnModel::Request &req, rodentrider_arena::SpawnModel::Response &res)
{
    return SpawnModel(req.model_name, req.spawn_name, req.x, req.y, req.z, req.yaw);
}

bool MyPlugin::RemoveModel(std::string model_name)
{
    this->world->RemoveModel(model_name);
    return true;
}

bool MyPlugin::ServiceRemoveModel(rodentrider_arena::RemoveModel::Request &req, rodentrider_arena::RemoveModel::Response &res)
{
    return RemoveModel(req.model_name);
}
